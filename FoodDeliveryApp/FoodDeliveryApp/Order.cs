﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDeliveryApp
{
    public class Order
    {
        private int _orderId;
        private string _itemName;
        private double _totalCost;

        public int GetOrderId()
        {
            return this._orderId;
        }

        public string GetItemName()
        {
            return this._itemName;
        }

        public double SetTotalCost()
        {
            return this._totalCost;
        }

        public void SetOrderId(int orderId)
        {
            this._orderId = orderId;
        }

        public void SetItemName(string itemName)
        {
            this._itemName = itemName;
        }

        public void SetTotalCost(double totalCost)
        {
            this._totalCost = totalCost;
        }
    }
}
