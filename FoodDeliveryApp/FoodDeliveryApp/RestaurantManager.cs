﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodDeliveryApp
{
    public class RestaurantManager 
    {
        private readonly Menu _menu;
        private List<string> _itemsBelowPrice;

        public RestaurantManager(Menu menu)
        {
            this._menu = menu;
        }

        public void DisplayManagerQueries()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nPlease select an operation to be performed: ");
            Console.WriteLine("1. Add an Item to the Menu.");
            Console.WriteLine("2. Get last 5 Orders.");
            Console.WriteLine("3. Get the most Popular Item.");
            Console.WriteLine("4. Get the item with Highest Revenue.");
            Console.WriteLine("5. Get items below the price.");
            Console.Write(">>");
            Console.ResetColor();

        }

        public void AddItem()
        {
            Console.WriteLine("Enter the name of the Dish/Item : ");
            var itemName = Console.ReadLine();
            Console.WriteLine("Enter the cost for {0}: ", itemName);
            double price = Convert.ToDouble(Console.ReadLine());

            this._menu.AddItem(itemName, price);
            Console.WriteLine("Item Added to Menu Successfully!");
            return;
        }

        public void ReceiveOrder(int itemId) {
            var itemIds = this._menu.GetItemIds();

            

            if (itemIds.Contains(itemId))
            {
                Console.WriteLine(this._menu.TakeOrder(itemId));
            }
            else
            {
                Console.WriteLine("Invalid Item!");
            }
        }

        public List<string> GetLastFiveOrders()
        {
            if (this._menu.GetTotalOrders() == 0)
                return null;
            else
            {
                var list = this._menu.Get_previousOrders();
                list.Reverse();
                var fiveOrders = list.Take(5).ToList<string>();
                return  fiveOrders;
            }
        }

        public void DisplayLastFiveOrders(List<string> previousOrders)
        {
            if(previousOrders == null)
            {
                Console.WriteLine("There are no previously placed orders!");
                return;
            }
            for(int i=0;i<previousOrders.Count; i++)
            {
                Console.WriteLine("{0}. {1}", (i + 1), previousOrders[i]);
            }
        }

        public string GetMostPopularItem()
        {
            if (this._menu.GetTotalOrders() == 0)
                return "There are no previous orders available!";

            int countOfOrders = 0;
            string mostPopularItem = "";
            foreach(Item item in this._menu.items)
            {
                if (item.Get_count() > countOfOrders)
                {
                    countOfOrders = item.Get_count();
                    mostPopularItem = item.Get_itemName();
                }
            }

            return mostPopularItem;
        }

        public string GetItemWithHighestRevenue()
        {
            if (this._menu.GetTotalOrders() == 0)
                return "There are no previous orders available!";

            double revenue = 0;
            string itemWithHighestRevenue = "";
            foreach (Item item in this._menu.items)
            {
                if (item.Get_totalRevenue() > revenue)
                {
                    revenue = item.Get_totalRevenue();
                    itemWithHighestRevenue = item.Get_itemName();
                }
            }

            return itemWithHighestRevenue;
        }

        public List<string> itemsBelowPrice(double price)
        {
            if (this._menu.Get_totalItems() == 0)
                return null;
            this._itemsBelowPrice = new List<string>();

            foreach(Item item in this._menu.items)
            {
                if (item.Get_price() <= price)
                {
                    this._itemsBelowPrice.Add(item.Get_itemName());
                }
            }

            return this._itemsBelowPrice;
        }

        public void DisplayItemsBelowPrice(List<string> items,double price)
        {
            if(items == null)
            {
                Console.WriteLine("There are no items below the price: {0}", price);
                return;
            }
            Console.WriteLine("Items below the price - {0} are:", price);
            for (int i = 0; i < items.Count; i++)
            {
                Console.WriteLine("{0}. {1}", (i+1), items[i]);
            }
        }

        
    }
}
