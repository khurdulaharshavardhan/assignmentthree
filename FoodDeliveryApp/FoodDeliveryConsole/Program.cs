﻿using System;
using FoodDeliveryApp;


namespace FoodDeliveryConsole
{
    class Program
    {
        private readonly Menu _menu;
        private readonly RestaurantManager _manager;
        private readonly Customer _customer;

        private int _userType;

        Program()
        {
            this._menu = new Menu();
            this._manager = new RestaurantManager(this._menu);
            this._customer = new Customer(this._manager, this._menu);
            this._userType = 0;
        }


        public void DisplayUserMenu()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\n---------------------------");
            Console.WriteLine("Please Select User-type: ");
            Console.WriteLine("1. Customer");
            Console.WriteLine("2. Restaurant Manager");
            Console.Write(">>");
            Console.ResetColor();
        }


        static void Main(string[] args)
        {
            //instantiating all real time objects and users.
            Program driver = new Program();


            while (true)
            {
                driver.DisplayUserMenu();
                try
                {
                    driver._userType = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid Input!");
                    continue;
                }
                

                switch (driver._userType)
                {
                    case 1: driver._customer.PlaceOrder();
                        break;

                    case 2: driver._manager.DisplayManagerQueries();
                        int choice = Convert.ToInt32(Console.ReadLine());

                        switch (choice)
                        {
                            case 1: driver._manager.AddItem(); 
                                    break;

                            case 2: var lastFiveOrders=driver._manager.GetLastFiveOrders();
                                    driver._manager.DisplayLastFiveOrders(lastFiveOrders);
                                    break;

                            case 3: var mostPopItem = driver._manager.GetMostPopularItem();
                                    Console.WriteLine("The most popularly known item is: {0}", mostPopItem);
                                    break;

                            case 4: var itemWithHighestRevenue = driver._manager.GetItemWithHighestRevenue();
                                    Console.WriteLine("The dish/item with highest revenue is: {0}", itemWithHighestRevenue);
                                    break;

                            case 5:
                                    Console.WriteLine("Please enter the price: ");
                                    double price = Convert.ToDouble(Console.ReadLine());

                                if (price <= 0)
                                    Console.WriteLine("Invalid Price input!");
                                else
                                {
                                    var item = driver._manager.itemsBelowPrice(price);
                                    driver._manager.DisplayItemsBelowPrice(item, price);
                                }
                                    break;

                            default: Console.WriteLine("Invalid Choice!"); break;
                        }
                            break;

                    default: Console.WriteLine("Invalid Choice!"); break;
                }

            }
        }
    }
}
