﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDeliveryApp
{
    public class Item
    {
        private int _serialNumber;

        public int Get_serialNumber()
        {
            return _serialNumber;
        }

        public void Set_serialNumber(int value)
        {
            _serialNumber = value;
        }

        private string _itemName;

        public string Get_itemName()
        {
            return _itemName;
        }

        public void Set_itemName(string value)
        {
            _itemName = value;
        }

        private double _price;

        public double Get_price()
        {
            return _price;
        }

        public void Set_price(double value)
        {
            _price = value;
        }

        private double _totalRevenue;

        public double Get_totalRevenue()
        {
            return _totalRevenue;
        }

        public void Set_totalRevenue(double value)
        {
            _totalRevenue = value;
        }

        private int _count;

        public int Get_count()
        {
            return _count;
        }

        public void Set_count(int value)
        {
            _count = value;
        }

        public Item() {
            this._count = 0;

            this._price = 0.0;

            this._serialNumber = 0;

            this._itemName = "";

            this._totalRevenue = 0.0;
        }
    }
}
