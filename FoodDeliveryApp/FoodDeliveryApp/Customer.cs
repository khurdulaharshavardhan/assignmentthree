﻿using System;

namespace FoodDeliveryApp
{
    public class Customer
    {
        private int _itemID;
        private readonly RestaurantManager _manager;
        private readonly Menu _menu;

        public Customer(RestaurantManager manager,Menu menu)
        {
            this._itemID = 0;
            this._manager = manager;
            this._menu = menu;
        }


        public void PlaceOrder()
        {
            
            
            if (this._menu.Get_totalItems() == 0)
            {
                Console.WriteLine("There are no items to Order!");
                return;
            }
            this._menu.ViewMenu();
            this._itemID = Convert.ToInt32(Console.ReadLine());
            this._manager.ReceiveOrder(this._itemID);
        }
    }
}
