﻿Feature: RestaurantManager
	The following scenarios are the queries that a RestaurantManager can perform.

Background: 
	Given: I am the Restaurant Manager.

Scenario: Get Last five Orders - Success
	Given the previously placed orders are "Biryani, Biryani, Dosa"
	When the Restuarant manager requests for the five previosly ordered items
	Then a list containing the previously ordered items is fetched
	And the response should be 'Dosa, Biryani, Biryani'


Scenario: Get Last five Orders - Failure
	When the Restuarant manager requests for the five previosly ordered items
	And there are no previously placed orders
	Then the response should be 'There are no previous orders available!'

Scenario: Get the Most Popular Item - Success
	Given the previously placed orders are "Biryani, Biryani, Dosa"
	When the Restuarant manager requests for the most frequently ordered item
	Then a list containing the previously ordered items is fetched
	And the response should be 'Biryani'

Scenario: Get the Most Popular Item - Failure
	When the Restuarant manager requests for the most frequently ordered item
	And there are no previously placed orders
	Then the response should be 'There are no previous orders available!'

Scenario: Get the Item with Highest Revenue - Success
	Given the previously placed orders are "Biryani, Biryani, Dosa"
	When the Restuarant manager requests for the item with highest revenue
	Then a list containing the previously ordered items is fetched
	And the response should be 'Biryani'

Scenario: Get the Item with Highest Revenue - Failure
	When the Restuarant manager requests for the item with highest revenue
	And there are no previously placed orders
	Then the response should be 'There are no previous orders available!'

Scenario: Get the Items below the Price - Success
	Given the input price of item 150
	When the Restaurant manager requests for the items below price
	And the menu contains two items below the mentioned price namely Biryani and Dosa
	Then the response should be 'Biryani, Dosa'

Scenario: Get the Items below the Price - Failure
	Given the input price of item 35
	When the Restaurant manager requests for the items below price
	And the menu is empty
	Then the response should be 'The Menu is currently empty!'