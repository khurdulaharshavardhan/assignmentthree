﻿using System.Collections.Generic;
using FluentAssertions;
using FoodDeliveryApp;
using TechTalk.SpecFlow;

namespace SpecFlowFoodDelApp.Steps
{
    [Binding]
    public class RestaurantManagerSteps
    {
        private readonly RestaurantManager _manager;
        private readonly Menu _menu;
        private List<string> _previousOrders;
        private List<string> _lastFiveOrders;
        private List<string> _allOrders;
        private List<string> _itemsBelowPrice;

        private string _result;
        private double _price;

        public RestaurantManagerSteps() {
            this._menu = new Menu();
            this._manager = new RestaurantManager(this._menu);

            this._menu.AddItem("Biryani",100);
            this._menu.AddItem("Dosa", 32);
            this._previousOrders = new List<string>();
            this._result = "";
            this._price = 0;
            this._itemsBelowPrice = new List<string>();
        }


        [Given(@"the previously placed orders are ""(.*)""")]
        public void GivenThePreviouslyPlacedOrdersAre(string p0)
        {
            var filteredString = p0.Replace(" ", "");
            var previousOrders = p0.Split(",");
            
            foreach (string item in previousOrders)
            {
                this._previousOrders.Add(item);
            }

            this._menu.TakeOrder(1);
            this._menu.TakeOrder(1);
            this._menu.TakeOrder(2);

        }



        [Given(@"the input price of item (.*)")]
        public void GivenTheInputPriceOfItem(double p0)
        {
            this._price = p0;
        }
        
        [When(@"the Restuarant manager requests for the five previosly ordered items")]
        public void WhenTheRestuarantManagerRequestsForTheFivePrevioslyOrderedItems()
        {
            var lastFiveOrders = this._manager.GetLastFiveOrders();

            if(lastFiveOrders == null)
            {
                this._result = "There are no previous orders available!";
            }
            else
            {
                this._result = string.Join(", ", lastFiveOrders);
            }
            
        }
        
        [When(@"there are no previously placed orders")]
        public void WhenThereAreNoPreviouslyPlacedOrders()
        {
            this._lastFiveOrders = null;
        }
        
        [When(@"the Restuarant manager requests for the most frequently ordered item")]
        public void WhenTheRestuarantManagerRequestsForTheMostFrequentlyOrderedItem()
        {
            this._result = this._manager.GetMostPopularItem();
        }
        
        [When(@"the Restuarant manager requests for the item with highest revenue")]
        public void WhenTheRestuarantManagerRequestsForTheItemWithHighestRevenue()
        {
            this._result = this._manager.GetItemWithHighestRevenue();
        }
        
        [When(@"the Restaurant manager requests for the items below price")]
        public void WhenTheRestaurantManagerRequestsForTheItemsBelowPrice()
        {
            var temp = this._manager.itemsBelowPrice(this._price);

            if (temp != null)
                this._itemsBelowPrice = this._manager.itemsBelowPrice(this._price);
            else
                this._result = "The Menu is currently empty!";
        }

       

        [When(@"the items is empty")]
        public void WhenTheMenuIsEmpty()
        {
            //the items is empty so the manager method returns null.
            this._result = "The Menu is currently empty!";
        }
        
        [Then(@"a list containing the previously ordered items is fetched")]
        public void ThenAListContainingThePreviouslyOrderedItemsIsFetched()
        {
            this._allOrders = this._menu.Get_previousOrders();
        }
        
        [Then(@"the response should be '(.*)'")]
        public void ThenTheResponseShouldBe(string p0)
        {
            _result.Should().Be(p0);
        }

        [When(@"the items contains two items below the mentioned price namely Biryani and Dosa")]
        public void WhenTheMenuContainsTwoItemsBelowTheMentionedPriceNamelyBiryaniAndDosa()
        {
            this._result = string.Join(", ", this._itemsBelowPrice);
        }
    }
}
