﻿using System;
using TechTalk.SpecFlow;
using FoodDeliveryApp;
using FluentAssertions;

namespace SpecFlowFoodDelApp.Steps
{
    [Binding]
    public class CustomerSteps
    {
        private readonly Menu _menu;
        private readonly RestaurantManager _manager;
        private readonly Customer _customer;
        private string _result;
        private int _itemId;

        public CustomerSteps()
        {
            this._menu = new Menu();
            this._manager = new RestaurantManager(this._menu);
            this._customer = new Customer(this._manager, this._menu);
            this._itemId = 0;
            this._result = "";
        }
        [When(@"the Menu containing list of items is displayed\.")]
        public void WhenTheMenuContainingListOfItemsIsDisplayed_()
        {
            
            this._menu.ViewMenu();
            
        }
        
        [When(@"the customer enters the item_id (.*)")]
        public void WhenTheCustomerEntersTheItem_Id(int p0)
        {
            this._itemId =p0;

        }

        [When(@"the list of items is not empty")]
        public void WhenTheListOfItemsIsNotEmpty()
        {
            this._menu.AddItem("Roti", 10);
            
        }


        [When(@"the list is empty")]
        public void WhenTheListIsEmpty()
        {
            var emptyMenu = new Menu();
            this._result = emptyMenu.TakeOrder(this._itemId);
        }
        
        [Then(@"the result should be '(.*)'")]
        public void ThenTheResultShouldBe(string p0)
        {
            this._result.Should().Be(p0);
        }
    }
}
