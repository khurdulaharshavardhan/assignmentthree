﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDeliveryApp
{
    public class Orders
    {
        private List<string> _previousOrdersByNames;
        private List<Order> _previousOrders;

        public Orders()
        {
            this._previousOrdersByNames = new List<string>();
            this._previousOrders = new List<Order>();
        }

        public List<string> GetPreviousOrdersByNames()
        {
            return this._previousOrdersByNames;
        }

        public void NewOrder(string itemOrdered, int itemId, double totalCost)
        {
            var order = new Order();
            order.SetItemName(itemOrdered);
            order.SetOrderId(itemId);
            order.SetTotalCost(totalCost);
            this._previousOrders.Add(order);
            this._previousOrdersByNames.Add(order.GetItemName());
        }
    }
}
