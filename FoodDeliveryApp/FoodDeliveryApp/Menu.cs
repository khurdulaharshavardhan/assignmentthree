﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDeliveryApp
{
   public class Menu 
    {
        private readonly Item _item;
        private readonly Orders _order;
        public List<Item> items;
        private List<int> _itemIds;
        

        private int _totalItems;

        public int Get_totalItems()
        {
            return this._totalItems;
        }

        public int GetTotalOrders()
        {
            return this._order.GetPreviousOrdersByNames().Count;
        }

        public List<string> Get_previousOrders()
        {
            return this._order.GetPreviousOrdersByNames();
        }

        public List<int> GetItemIds()
        {
            return this._itemIds;
        }

        public Menu() {
            this._item = new Item();
            this.items = new List<Item>();
            this._totalItems = 0;
            this._order = new Orders();
            this._itemIds = new List<int>();
        }
     
        public void AddItem(string itemName, double price)
        {
            this._totalItems++;
            Item tempItem = new Item();

            //init the values:
            tempItem.Set_itemName(itemName);
            tempItem.Set_price(price);      
            tempItem.Set_serialNumber(this._totalItems);

            this._itemIds.Add(tempItem.Get_serialNumber());

            var count = tempItem.Get_count();

            count++;
            tempItem.Set_count(count);

            tempItem.Set_totalRevenue(0);

            this.items.Add(tempItem);
            return;
        }


        public string TakeOrder(int id)
        {
            if(this._totalItems == 0)
            {
                return "There are no items to Order!";
                
            }

            foreach(Item item in this.items)
            {
                if (item.Get_serialNumber() != id)
                {
                    continue;
                }
                else
                {
                    var count = item.Get_count();
                    count++;
                    var revenue = item.Get_price() * count ;

                    item.Set_count(count);
                    item.Set_totalRevenue(revenue);
                    this._order.NewOrder(item.Get_itemName(), item.Get_serialNumber(), item.Get_price());


                    return "Your Order has been placed Successfully!";
                }
            }
            return "Invalid Item!";
        }

        public void ViewMenu()
        {
            if(this._totalItems == 0)
            {
                Console.WriteLine("There are no items to Order!");
                return;
            }
            else
            {
                Console.WriteLine("Today's items:");
                foreach(Item item in this.items)
                {
                    Console.WriteLine("{0}. {1} -- Rs.{2}", item.Get_serialNumber(), item.Get_itemName(), item.Get_price());
                }
                Console.WriteLine("Please Enter your choice: ");
            }

        }

    }
}
