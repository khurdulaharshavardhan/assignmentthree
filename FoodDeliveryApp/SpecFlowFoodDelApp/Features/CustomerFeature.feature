﻿Feature: Customer
	This feature describes the functionality of the Customer Entity.

Background: 
	Given: I am the Customer.

Scenario: Placing an Order - Success
	When the Menu containing list of items is displayed.
	And the list of items is not empty
	When the customer enters the item_id 1
	Then the result should be 'Your Order has been placed Successfully!'

Scenario: Placing an Order - Failure
	When the Menu containing list of items is displayed.
	When the list is empty
	And the customer enters the item_id 1
	Then the result should be 'There are no items to Order!'

Scenario: Placing an Order - Failure - Invalid Item id
	When the Menu containing list of items is displayed.
	And the list of items is not empty
	When the customer enters the item_id 3
	Then the result should be 'Invalid Item!'

